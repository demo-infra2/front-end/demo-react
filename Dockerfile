# Usa una imagen base, por ejemplo, el servidor de desarrollo de React
FROM node:16 AS build
WORKDIR /app

# Copia los archivos del microfrontend React
COPY package*.json .

RUN npm install

COPY . .

# Construye la aplicación React
RUN npm run build

# Segunda etapa para una imagen más pequeña
FROM nginx:alpine

COPY ./nginx.conf /etc/nginx/nginx.conf

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

# Copia el resultado de la compilación de React desde la etapa anterior
COPY --from=build /app/dist /usr/share/nginx/html

# Expone el puerto en el cual el microfrontend React estará disponible
EXPOSE 8080
