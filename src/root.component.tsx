import { useState, useEffect } from "react";
import apiService from './services/apiService';


export default function Root(props) {

  const [data, setData] = useState({ code: null, message: null });

  useEffect(() => {
    obtenerDatos();
  }, []);

  const obtenerDatos = async () => {
    try {
      const datosObtenidos = await apiService.obtenerDatos();
      setData(datosObtenidos);
    } catch (error) {
      // Manejo del error
      console.error('Error al obtener datos:', error);
    }
  };

  return (
    <section style={{ textAlign: 'center', marginTop: '50px', fontFamily: 'Arial, sans-serif' }}>
      <h1 style={{ color: 'blue' }}>Esta es la aplicación de React</h1>
      <img
        src="https://blog.wildix.com/wp-content/uploads/2020/06/react-logo.jpg"
        alt="React Logo"
        style={{ width: '300px', height: 'auto', marginBottom: '20px' }}
      />
      <div style={{ margin: '20px', padding: '20px', border: '1px solid #ccc', borderRadius: '5px' }}>
        <h3>Mensaje recibido del API:</h3>
        <p style={{ fontSize: '18px', fontWeight: 'bold', color: 'green' }}>{data.message}</p>
      </div>
    </section>
  );


}
