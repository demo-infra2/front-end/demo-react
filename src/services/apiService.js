class apiService {


    async getToken() {
        // Recorremos todas las claves almacenadas en el localStorage
        for (let i = 0; i < localStorage.length; i++) {
            // Verificamos si la clave termina con 'accessToken' y si contiene el clientId adecuado
            if (localStorage.key(i).endsWith('accessToken') && localStorage.key(i).includes('53k4nc2k6920145iv0ldo6tmi6')) {
                // Si encontramos una coincidencia, retornamos el token almacenado
                return localStorage.getItem(localStorage.key(i));
            }
        }
        // Si no encontramos ningún token válido, retornamos null
        return null;
    }


    async obtenerDatos() {
        try {
            const token = await this.getToken();
            const response = await fetch("http://localhost:8082/api-demo/demo/react", {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });


            if (!response.ok) {
                throw new Error('Error al obtener datos');
            }
            const data = await response.json();
            return data;
        } catch (error) {
            console.error('Error al obtener datos:', error);
            throw error; // Propaga el error para manejarlo en el componente que llama al servicio
        }
    }
}

export default new apiService();
